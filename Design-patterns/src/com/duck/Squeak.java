package com.duck;

public class Squeak implements QuackBehavior{
    public void quack(){
        System.out.println("rubber duckie squeak");
    }
}