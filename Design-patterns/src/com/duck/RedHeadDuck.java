package com.duck;

import com.patterns.Duck;

public class RedHeadDuck extends Duck {
    
    /**
     * Like this method is inherit from the abstract main class Duck
     * I have to show the same method's name without abstract
     * 
     */

    public void display(String face){
        System.out.println("The duck is "+face);
    }
}
