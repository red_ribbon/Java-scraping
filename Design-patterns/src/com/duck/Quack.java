package com.duck;

public class Quack implements QuackBehavior, FlyBehavior{
    public void quack(){
        System.out.println("implements duck quacking");
    }
    public void fly(){}
}
    