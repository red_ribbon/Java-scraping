package com.duck;

import com.patterns.Duck;


public class MallarDuck extends Duck{

    public MallarDuck(){

        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }

    /**
     * Like this method is inherit from the abstract main class Duck
     * I have to show the same method's name without abstract
     * 
     */
    public void display(){
        System.out.println("I'm a real Mallar Duck");
    }
    
    /*protected void quack(String sound){
        System.out.println("This sound: "+sound);
    }*/
}
