package com.company;

import com.duck.MallarDuck;
import com.duck.RedHeadDuck;
//import com.patterns.Duck;
import com.duck.RubberDuck;


public class Main {


    public static void main(String[] args){

        /*Duck pato = new Duck();
        pato.quack();*/

        MallarDuck patomalo = new MallarDuck();
        RedHeadDuck rojo = new RedHeadDuck();
        RubberDuck goma = new RubberDuck();

        
        patomalo.display("looks like a mallard");
        //patomalo.quack();

        rojo.display("Looks like a redhead");
        //rojo.quack();
        //System.out.println("Design patterns...");
    }
}