package com.patterns;

import com.duck.FlyBehavior;
import com.duck.QuackBehavior;

public abstract class Duck {
    
    //protected String Type ;
    // calling instance from interface FlyBehavior and QuackBehavior
    public FlyBehavior flyBehavior;
    public QuackBehavior quackBehavior;
    public Duck(){}
    
    
    /*
     * 
     * 
     * 
     */
    public void PerformQuack(){
        quackBehavior.quack();
    }
    public void swim(){
        System.out.println("All the ducks are floating.");
    }


    public void PerformFly(){
        flyBehavior.fly();
    }





    // Abstract method has not body, only parameters
    public abstract void display();

}
