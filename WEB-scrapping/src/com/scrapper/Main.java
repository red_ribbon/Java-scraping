package com.scrapper;

import java.util.*;
import java.net.*;
import java.io.*;



/**
 *@autor Arturo Negreiros Samanez. 
 *
 * */

public class Main {

    List Country = new ArrayList();
    
    List Deaths = new ArrayList();
    

    Dictionary Acumulatives = new Hashtable();
    

    public static final String covid = "https://covid19.who.int/WHO-COVID-19-global-data.csv";
    public static final String fileName = "WHO-COVID-19-global-data.csv";


    public void GetData(){
     
        try(BufferedInputStream in = new BufferedInputStream(new URL(covid).openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(fileName)){
            
            byte dataBuffer [] = new byte[1024];
            int bytesRead;

            System.out.println("[*] Scrapper is ready...");
            
            while ((bytesRead = in.read(dataBuffer, 0,1024)) != -1 ){
                fileOutputStream.write(dataBuffer, 0, bytesRead);
            }

            System.out.println("[*] Scrapper finished successfully.");

        }catch(Exception e){
            System.out.println("Error by"+ e.toString());
        }
        
    }

    public void storeData(){

        try {

            System.out.println("[*]Reading data...");
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String strLine = "";
            
            while ((strLine = br.readLine()) != null ){
                String [] splited = strLine.split(",");
                if (Country.contains(splited[2]) == false){

                    Country.add(splited[2]);
                }
                
            }
            Country.remove(0);
            br.close();
            
        } catch(Exception ex){
            System.out.println("Error reading data by: "+ex.toString());
        }
    }

    public void read (){

        /**
         * Get size of country array
         *
         *
         * */
        System.out.println(Country.size());
        
        for (int x = 0; x < Country.size(); x++){
            System.out.println(Country.get(x));
        }
    }

    public void addCountry(String country){

        try {
            BufferedReader bfr = new BufferedReader(new FileReader(fileName));
            String buffer = "";
            List Cases = new ArrayList();

            while ((buffer = bfr.readLine()) != null){
                String[] lines = buffer.split(",");


                if (lines[2].equals(country)){
                    Cases.add(lines[4]);
                }
            }
            bfr.close();
            Acumulatives.put(country, Cases);

        }catch(Exception e ){
            System.out.println("Troubles in: "+e.toString());
        }

    }


    public void  getCases(){
        System.out.println("Storing into Dictionaries...");
        
        for (int x =0 ; x < Country.size(); x++){
            addCountry(Country.get(x).toString());
        }   

        //readDictionary();
        //System.out.println(Acumulatives.size());
    }

    /*public void readDictionary(){

        System.out.println(Acumulatives);
    }*/

    public void readCountries(){

        System.out.println(Country.size());
        
        /*for (int x = 0; x < Country.size(); x ++){
            System.out.println(Country.get(x));
            
        }*/
        
        for (int y = 0; y< Country.size(); y++ ){
            System.out.println(Country.get(y) + " => "+Acumulatives.get(Country.get(y)));
        }
    }


    public static void main (String[] args) throws Exception{

        Main main = new Main();
        
        main.GetData();
        main.storeData();
        main.read();
        main.getCases();
        main.readCountries();
       // main.readDictionary();
    }

}
